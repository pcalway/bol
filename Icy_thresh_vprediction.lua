-->>            Thresh Hook             <<--

if myHero.charName ~= "Thresh" then return end
require "VPrediction"
	
local qRange = 1075
local wRange = 950
local eRange = 450
local QREADY, EREADY = false, false

if VIP_USER then
	require "Collision"
	Col = Collision(qRange, 1200, 0.5, 80)
	VP = VPrediction()
	-- tp = ProdictManager.GetInstance()
	-- qp = tp:AddProdictionObject(_Q, qRange, 1200, 0.5, 80)
	PrintChat("<font color='#CCCCCC'> >> Thresh Hook - Prodiction <<</font>")
else
	qp = TargetPrediction(qRange, 1.2, 500, 80)
	PrintChat("<font color='#CCCCCC'> >> Thresh Hook - Basic Prediction <<</font>")
end
 
function OnLoad()
	ThreshConfig = scriptConfig("ThreshHook", "ThreshHook")
	ThreshConfig:addParam("HooknPull", "Hook n Pull", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	ThreshConfig:addParam("PullnPassage", "Pull n Passage", SCRIPT_PARAM_ONKEYDOWN, false, 71)
	ThreshConfig:addParam("pullMode", "Pull Mode", SCRIPT_PARAM_SLICE, 1, 1, 2, 0)
	ThreshConfig:addParam("drawCircles", "Draw Ranges", SCRIPT_PARAM_ONOFF, false)
	ThreshConfig:permaShow("HooknPull")
	ThreshConfig:permaShow("PullnPassage")

	ts = TargetSelector(TARGET_LOW_HP_PRIORITY, 1200, DAMAGE_PHYSICAL, true)
	ts.name = "Thresh"
	ThreshConfig:addTS(ts)
	enemyMinions = minionManager(MINION_ENEMY, 1400, myHero)
end
 
function OnTick()
	ts:update()
	enemyMinions:update()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	if ts.target then
		if ThreshConfig.HooknPull then HooknPull() end
		if ThreshConfig.PullnPassage then PullnPassage() end
	end
end

function HooknPull()
	-- local predic = qp:GetPrediction(ts.target)
	local CastPosition, HitChance, Position = VP:GetLineCastPosition(ts.target, 0.5, 80, 1075, 1200, myHero)
	local spellQ = myHero:GetSpellData(_Q).name
	if CastPosition and GetDistance(CastPosition) <= qRange and HitChance > 1 and QREADY and spellQ == "ThreshQ" then
		if VIP_USER then
			if not Col:GetMinionCollision(myHero, CastPosition) then CastSpell(_Q, CastPosition.x, CastPosition.z) end
		else 
			CastSpell(_Q, CastPosition.x, CastPosition..z) 
		end
	end
	if spellQ == "threshqleap" then CastSpell(_Q) end
	if GetDistance(ts.target) <= eRange and EREADY then
		xPos = myHero.x + (myHero.x - ts.target.x)
		zPos = myHero.z + (myHero.z - ts.target.z)
		CastSpell(_E, xPos, zPos)
	end
end
 
function PullnPassage()
	--local predic = qp:GetPrediction(ts.target)
	local predic, HitChance, Position = VP:GetLineCastPosition(ts.target, 0.5, 80, 1075, 1200, myHero)
	local castW, castQ = false, false
	local spellQ = myHero:GetSpellData(_Q).name
	local LanternAlly = nil
	if ThreshConfig.pullMode == 1 then
		LanternAlly = findClosestAlly()
	else
		LanternAlly = findFurthestAlly()
	end
	if predic and GetDistance(predic) <= qRange and QREADY and HitChance > 1 and spellQ == "ThreshQ" then
		if VIP_USER then
			if not Col:GetMinionCollision(myHero, predic) then
				CastSpell(_Q, predic.x, predic.z)
			end
		else
			CastSpell(_Q, predic.x, predic.z)
		end
	end
	if spellQ == "threshqleap" then 
		castW = true
		if castQ == true or not WREADY then CastSpell(_Q) end
	end
	if LanternAlly and GetDistance(LanternAlly) < wRange and WREADY and castW then
		CastSpell(_W, LanternAlly.x, LanternAlly.z)
		castQ = true
		castW = false
	end
	if GetDistance(ts.target) <= eRange and EREADY then
		xPos = myHero.x + (myHero.x - ts.target.x)
		zPos = myHero.z + (myHero.z - ts.target.z)
		CastSpell(_E, xPos, zPos)
	end
end

function OnDraw()
	if not myHero.dead and ThreshConfig.drawCircles then
		DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0xCCFF33)
	end
end
 
function findClosestAlly()
	local closestAlly = nil
	local currentAlly = nil
	for i=1, heroManager.iCount do
		currentAlly = heroManager:GetHero(i)
		if currentAlly.team == myHero.team and not currentAlly.dead and currentAlly.charName ~= myHero.charName then
			if closestAlly == nil then
				closestAlly = currentAlly
			elseif GetDistance(currentAlly) < GetDistance(closestAlly) then
				closestAlly = currentAlly
			end
		end
	end
	return closestAlly
end

function findFurthestAlly()
	local furthestAlly = nil
	local currentAlly = nil
	for i=1, heroManager.iCount do
		currentAlly = heroManager:GetHero(i)
		if currentAlly.team == myHero.team and not currentAlly.dead and currentAlly.charName ~= myHero.charName and GetDistance(currentAlly) <= wRange then
			if furthestAlly == nil then
				furthestAlly = currentAlly
			elseif GetDistance(currentAlly) > GetDistance(furthestAlly) then
				furthestAlly = currentAlly
			end
		end
	end
	return furthestAlly
end